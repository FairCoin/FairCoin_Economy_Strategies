Voir ci-dessous pour la version en français / Siehe weiter unten für die deutsche Version
 
Comments, questions and answers can be found here: https://board.net/p/fairo-porposal-questions
Illustrations of different Fairo scenarios: https://board.net/p/fairo-porposal-scenarios
Separate FairPoints proposal: https://board.net/p/fairpoints


# Fairo Proposal

This proposes an evolution of the Fairo as it currently exists here: https://fairo.exchange/ and has been experimented with in the fair economy between the FairCoop local nodes in Switzerland.
 
This proposal suggests that it is time to disconnect the core of the FairCoin economy more strongly from the financial system we aim to replace. As long as we keep thinking of our fair economy in terms of the Euro as the central measure for value, we remain tied-in too directly into the tensions of the systemic reality around us. Instead of the dependency of our fair economy on an exchange rate to the Euro, we shift our thinking towards Fairo as an independent measure for value. 
 
The Fairo is a proposed unit of measure for purchasing power. The value that 1 Fairo represents is based on the purchasing power of 20 FairCoin at the end of the year 2016, but more importantly should be thought of as equivalent to one thousandth of the mean future basic cost of living for one month. While the value of a Fairo is stable, we would fix consensus rates for the exchange between Fairo and FairCoin within the web of trust of FairCoop local nodes. Therefore there can be a diversity of rates for different economic realities in different areas. 
 
For setting Fairo rates, calculated bands of market tracking would be followed, either by directly linking the Fairo-FairCoin exchange rate through consensus to these calculated bands, or by more loosely following these bands as guiding best practices when adjusting exchange rates in consensus assemblies. 
 
A principle of the Fairo rates would be that they value FairCoin above the perceived market value outside the fair economy. This gives a competitive advantage to market participants that spend FairCoin in the fair economy and creates a disadvantage when FairCoin is spent or exchanged outside the fair economy. This is one way how FairCoin can serve as a membrane to protect the fair economy from the value extraction of the capitalist system.
The gap that can be maintained between the valuation of FairCoin in the fair economy and that outside in the capitalist market needs to be kept relative to the strength of the web of trust in the fair economy. If that gap gets too big, the web of trust will break down. Also, when the web of trust is weakened, the gap that can be maintained needs to be reduced. It is difficult to measure the right gap that must be maintained for proper functioning in balance with the strength of the web of trust, which is why the development of a good governance culture is important.  
 
The Fairo rate of FairCoin would be adjusted up with the market, but also corrected down. Rather than exposing the fair economy to the full fluctuation of the market, it would hover above a mid or short-term mean of the market, similar to what is suggested with the FreeVision proposal’s ask price. 
The Fairo rate would be used for FairCoin transactions inside the fair economy and for the exchange between FairCoin and other social currencies and mutual credit systems. This would represent a major shift in thinking about the value of contributions and resources in the fair economy in general and the value of FairCoin in particular. Transacting in Fairo frees us from the frame of mind imposed by the central bank money system and the competitive nature of the different economies in the world. Since the value of FairCoin is difficult to measure, it should instead be thought of in terms of the clearly understandable concept of the monthly cost of living. This allows us to deal with different local conditions and economic realities in a much more natural and relaxed way. A key for developing a healthy fair economy.
 
The problem in our current fair economy reality with FairCoin is the need for justifying the ambiguity of how FairCoin is valued, the gap between different exchange rates and the need to accept a limited liquidity in exchange markets. With the Fairo, these differences in valuation become a correction of the wrongly defined valuation with central bank money. With the Fairo we are able to turn these problems into virtues of the way our fair economy functions. A reality we subconsciously always knew is true but without the Fairo was difficult to explain and justify.


### Sustainable sources of exchange

To some extent we would want fair economy market participants to be able to exchange FairCoin to central bank money for covering fair external needs. Offering the ability to exchange to unfair currencies is only sustainable to the extent that the fair economy is able to extract value from the capitalist system. Sustainable ways of doing so are:

a) Fair economy participants offering services to the unfair economy through tools like FreedomCoop can contribute collected fees for funding exchange needs.
b) People with a life reality and stable income in the unfair economy but with interest and sympathies for the fair economy can contribute a monthly amount of unfair currency for exchange needs, in exchange for FairCoin that they intend to spend in the fair economy.
c) Fair investors that give unfair currency to buy FairCoin with no intention of ever exchanging back to an unfair currency, planning instead to invest the FairCoins into fair economy projects.

Unfair money received from such exchanges should be split between the use for fair redemption and the purchase of FairCoin at a low price from the unfair market.

### Balancing fair exchange through exchange bands (FairPoints Proposal)

This describes a proposed way of calculating exchange rates between any fair currency and unfair currencies (like central bank money):

Primarily, it would be the market conditions that limit the ability to provide exchange to unfair currency, but we would also want to differentiate access to exchanges based on the fairness of market participants, in order to encourage ethical and fair market behaviour. One simple way of measuring this fairness could be achieved with the addition of a FairPoint system as proposed here: The more FairPoints a market participant has, the higher the exchange rate they have access to when exchanging to an unfair currency. The concept is simple enough, so that it could be started in a way that relies on self-assessment of the market participants, being left to self regulate through peer review.

The FairPoints are divided into 3 fairness categories. For each of these categories there are 3 criteria. For each category one can get from 0 to 2 points: 

Fairness Category A
- Criteria a)
- Criteria b)
- Criteria c)

Fairness Category B
- Criteria a)
- Criteria b)
- Criteria c)

Fairness Category C
- Criteria a)
- Criteria b)
- Criteria c)

Point count: 
For A,B and C:
- 1 count for 2 out of 3
- 2 counts for all 3
for a total maximum of 6 counts

FairPoints are calculated by dividing the total counts by 2 and rounding down for a FairPoint rating between 0 and 3. 

Market participants with 3 FairPoints get to exchange at 90% of the Fairo rate when changing back to an unfair currency. Those with 2 FairPoints at 80% and those with 1 FairPoint at 70%. Those with 0 FairPoints cannot exchange money within the trusted network and must do so on the external market. 

Market participants with 3 FairPoints get to buy FairCoin at 70% of the Fairo rate when converting their Euros. Those with 2 FairPoints at 80% and those with 1 FairPoint at 90% and those with 0 FairPoint at full value, so 100%.  

This still requires a functioning web-of-trust in the fair economy in order to prevent any large scale unfair acceptance of cheap faircoins purchased outside the fair economy.
With an enhanced block explorer, we could over time increase transparency and hence lower the need for relying on the web of trust itself.

Separate pad for developing the FairPoints proposal: https://board.net/p/fairpoints


## Fairo Proposal 1

The current Fairo rate would be adjusted down to be 1 FairCoin == 1 Fairo. 

Technically, this already works in the current mobile and desktop wallets, as the Fairo is already listed as an available currency in the wallets. It’s just the rate that would be changed to 1:1 through the already implemented API.

The future rate would be set in consensus assemblies like it has been the case for the official rate so far. Additional exchange rates between Fairo and other currencies than FairCoin would only be set in a decentralised way, through local nodes or coordination between multiple local nodes in a bio region.

## Fairo Proposal 2

The current Fairo rate would be adjusted to a floating mean 25% above market value. 

Technically, this already works in the current mobile and desktop wallets, as the Fairo is already listed as an available currency in the wallets. It’s just the rate that would be changed to 25% above market through the already implemented API. That API would need to be programmed to calculate the floating mean 25% above the market price based on ask price of the past few days and weeks, balancing out short term pumps and dumps.

The formula and percentage level would be set in consensus assemblies like it has been the case for the official rate so far. Additional exchange rates between Fairo and other currencies than FairCoin would only be set in a decentralised way, through local nodes or coordination between multiple local nodes in a bio region.


## Fairo Proposal 3

The current Fairo rate would follow a mean between 1 FairCoin == 1 Fairo and a floating mean 25% above market value until the floating mean 25% above market value is higher than 1 FairCoin == 1 Fairo. Above 1 FairCoin == 1 Fairo it would then be the same as Fairo Proposal 2.

Technically, this already works in the current mobile and desktop wallets, as the Fairo is already listed as an available currency in the wallets. It’s just the rate that would be changed to the calculated value through the already implemented API. That API would need to be programmed to calculate the mean between 1 FairCoin == 1 Fairo and a floating mean 25% above market value or the floating mean 25% above the market price, whichever is higher, based on ask price of the past few days and weeks.

The formula and percentage level would be set in consensus assemblies like it has been the case for the official rate so far. Additional exchange rates between Fairo and other currencies than FairCoin would only be set in a decentralised way, through local nodes or coordination between multiple local nodes in a bio region.




Français:
# Proposition du Fairo

Cette proposition propose une évolution du Fairo tel qu'il existe actuellement ici : https://fairo.exchange/ et a été expérimenté dans l'économie équitable entre les nœuds locaux FairCoop en Suisse.  

Cette proposition suggère qu'il est temps de déconnecter plus fortement le cœur de l'économie du FairCoin du système financier que nous voulons remplacer. Tant que nous continuerons à penser notre économie équitable en termes d'euro et à nous référer à ce dernier entant que mesure centrale de la valeur, nous resterons trop directement sousmis aux tensions de la réalité systémique qui nous entoure. Au lieu de faire dépendre notre économie équitable d'un taux de change par rapport à l'euro, nous nous tournons donc vers le Fairo comme mesure indépendante de la valeur. 

Le Fairo est une unité de mesure proposée pour le pouvoir d'achat. La valeur que représente 1 Fairo est basée sur le pouvoir d'achat de 20 FairCoin à la fin de l'année 2016, mais surtout doit être considérée comme équivalente à un millième du futur coût de la vie moyen d'une personne pendant un mois. Bien que la valeur d'un Fairo soit stable, nous fixerions des taux consensuels pour l'échange entre Fairo et FairCoin au sein du réseau de confiance des nœuds locaux FairCoop. Par conséquent, il peut y avoir une diversité de taux pour différentes réalités économiques dans différentes régions. Pour fixer les taux de change du Fairo, nous suivrions le marché dans des fourchettes calculées, soit en reliant directement le taux de change Fairo-FairCoin par consensus avec ces fourchettes calculées, soit en utilisant de manière plus libre ces fourchettes comme guide des meilleures pratiques pour ajuster par consensus les taux de change lors de réunions.

L'un des principes des taux du Fairo serait qu'ils évaluent le FairCoin au-dessus de la valeur marchande perçue en dehors de l'économie équitable.Cela donne un avantage compétitif à ceux qui participent au marché équitable et dépensent leurs FairCoins dans l'économie solidaire et crée un désavantage lorsque cette monnaie est dépensée ou échangée en dehors de l'économie équitable. C'est une des façons par la-quelle les FairCoins peuvent servir de membrane pour protéger l'économie équitable de l'extraction, par le système capitaliste, de la valeur ajoutée.
L'écart qui peut être maintenu entre l'évaluation de la valeur du FairCoin dans l'économie équitable et celle à l'extérieur dans le marché capitaliste, doit être maintenu en fonction de à la force du réseau de confiance au sein de l'économie équitable. Si cet écart devient trop grand, le réseau de confiance s'effondrera. De même, lorsque le réseau de confiance est affaibli, l'écart qui peut être maintenu doit être réduit. Il est difficile de mesurer le bon écart qui doit être maintenue pour un bon fonctionnement, c'est  pourquoi le développement d'une bonne culture de gouvernance est important.

Le taux entre FairCoin et Fairo serait ajusté en cas de hausse du marché et également corrigé en cas baisse.  Mais au lieu d'exposer l'économie équitable à la pleine volatilité du marché, elle flotterait au-dessus d'une moyenne du marché à moyen ou à court terme, semblable à ce qui a été proposé avec le "Ask-Price" dans la proposition FreeVision. 

Le taux du Fairo serait utilisé pour les transactions de FairCoin au sein de l'économie équitable et pour l'échange entre FairCoin et d'autres monnaies sociales et systèmes de crédit mutuel. Cela représenterait un changement majeur dans la réflexion sur la valeur des contributions et des ressources dans l'économie équitable en général et en particulier de la valeur du FairCoin. Les transactions en Fairo nous libèrent de l'état d'esprit imposé par le système monétaire des banques centrales et de la nature compétitive des différentes économies du monde. Puisque la valeur du FairCoin est difficile à mesurer cela fait sens de l'envisager en termes du concept clairement compréhensible du coût de la vie mensuel. Cela nous permet de faire face aux différentes conditions et réalités économiques locales d'une manière beaucoup plus naturelle et détendue. Une clé pour développer une économie saine et équitable.  

Le problème dans la réalité actuelle de l'économie équitable avec le FairCoin est la nécessité de justifier l'ambiguïté de la façon dont le FairCoin est évalué, l'écart entre les différents taux de change et la nécessité d'accepter une liquidité limitée sur les marchés d'échanges. Avec le Fairo, ces différences de valorisation du FairCoin devient une correction du type de valorisation usuel sur le marché. Avec le Fairo, nous sommes en mesure de transformer ces problèmes en vertus du fonctionnement de notre économie équitable. Une réalité que nous avons inconsciemment toujours su être vraie, mais qui sans le Fairo était difficile à expliquer et à justifier.


### Sources d'échanges durables  

Dans une certaine mesure, nous voudrions que les participants au marché de l'économie équitable soient capables d'échanger des FairCoins contre de la monnaie de banque centrale pour couvrir des besoins externes justes. Offrir la possibilité d'échanger en devises injustes n'est viable que dans la mesure où l'économie équitable est capable d'extraire de la valeur du système capitaliste. Les moyens durables d'y parvenir le sont :  
    
    a) Des participants de l'économie équitable qui offrent des services à l'économie déloyale, en utilisant des outils comme FreedomCoop, pourraient contribuer au besoins d'échanges et de financements par le biais de redevances. 
    b) Des personnes avec une réalité de vie et un revenu stable dans l'économie inéquitable qui ont un l'intérêt et de la sympathie pour l'économie équitable, peuvent contribuer avec un montant mensuel en échange de FairCoins qu'ils comptent dépenser dans l'économie équitable.  
    c) Des investisseurs équitables qui achettent des FairCoins avec des devises sans avoir l'intention de les échanger contre des devises injustes, planifiant plutôt d'investir les FairCoins dans des projets d'économie équitable.  

L'argent injuste provenant de tels échanges, devrait être d'une part utilisés pour couvrir les besoins d'échanges au sein de l'économie équitable et d'autre part pour l'achat de FairCoins à un bas prix sur le marché inéquitable.


### Équilibre des échanges équitables par le biais de fourchettes d'échange (Proposition FairPoints)  

Il s'agit d'une méthode proposée pour calculer les taux de change entre n'importe quelle monnaie équitable et les monnaies injustes (comme l'argent issu des banques centrales):  

En premier lieu, ce seraient les conditions du marché qui limiteraient la capacité d'échanger contre des devises injustes, mais nous souhaitons aussi différencier l'accès aux échanges en fonction du niveau d'équité des participants au marché, afin d'encourager un comportement éthique et équitable sur le marché. Une façon simple de mesurer ce niveau d'équité pourrait être par le biais d'un système de FairPoints tel que proposé ici dessous: Plus un participant du marché a de FairPoints, plus le taux de change auquel il a accès, lorsqu'il change des Faircoins en une devise injuste, est élevé. Le concept est assez simple pour qu'il puisse être lancé d'une manière qui repose sur l'auto-évaluation des participants au marché, en étant laissé à l'autorégulation par le biais d'un examen par les pairs.  

Les FairPoints sont divisés en 3 catégories d'équité. Pour chacune de ces catégories, il y a 3 critères. Pour chaque catégorie on peut obtenir de 0 à 2 points :  

Catégorie d'équité A 
- Critère a) 
- Critère b) 
- Critère c)  

Catégorie d'équité B 
- Critère a) 
- Critère b) 
- Critère c)  

Catégorie d'équité C 
- Critère a) 
- Critère b) 
- Critère c)  

Nombre de points : 
Pour A, B et C : 
    - 1 compte pour 2 sur 3 
    - 2 comptes pour les 3 
pour un total de maximum 6 points  

Les FairPoints sont calculés en divisant le total des points par 2 et en arrondissant à l'unité inférieure pour obtenir une cote de FairPoints entre 0 et 3.  

Les participants du marché avec 3 FairPoints peuvent échanger à 90% du taux de change du Fairo lorsqu'ils échangent leurs devises justes contre une devise injuste. Ceux avec 2 FairPoints à 80% et ceux avec 1 FairPoint à 70%. Ceux ayant 0 FairPoints, ne peuvent pas échanger d'argent au sein du réseau de confiance et doivent le faire sur le marché exterieur. 

Les participants au marché avec 3 FairPoints peuvent acheter des FairCoin à 70% du taux du Fairo. Ceux avec 2 FairPoints à 80%, ceux avec 1 FairPoint à 90% et ceux avec 0 FairPoint à la pleine valeur, donc 100%.  

Cela nécessite toujours un réseau de confiance fonctionnel au sein de l'économie équitable afin d'empêcher que soient accéptés à grande échelle des FairCoins bon marché provenant du marché extérieur et donc d'être échangés au sein de l'économie équitable de manière injuste.
Avec un logiciel d'exploration de la blockchain amélioré, nous pourrions, avec le temps, accroître la transparence et ainsi réduire le besoin de nous fier au réseau de confiance lui-même, car nous pourions ainsi retracer plus facilement la provenance des FairCoins damandés à être échangés.

Pad séparé pour l'élaboration de la proposition FairPoints : https://board.net/p/fairpoints   


## Fairo: Proposition 1  

Le taux actuel de Fairo serait ajusté à 1 FairCoin == 1 Fairo.  

Techniquement, cela fonctionne déjà dans les portemonnaies mobiles et ordinateurs actuels, car le Fairo est déjà listé comme une devise disponible dans les portemonnaies. C'est juste le taux qui serait changé à 1:1 grâce à l'API déjà implémentée.  

Le taux futur serait fixé dans des assemblées par consensus, comme cela a été le cas pour le taux officiel jusqu'à présent. Les taux de change supplémentaires entre le Fairo et d'autres monnaies que le FairCoin ne seraient fixés que de manière décentralisée, par les nœuds locaux ou par le biais d'une coordination entre plusieurs nœuds locaux dans une biorégion.  

## Fairo: Proposition 2  

Le taux actuel de Fairo serait ajusté à une moyenne flottante de 25 % au-dessus de la valeur marchande.  

Techniquement, cela fonctionne déjà dans les portemonnaies mobiles et ordinateurs actuels, car le Fairo est déjà listé comme une devise disponible dans les portemonnaies. C'est juste le taux de valeur entre FairCoin et Fairo qui serait changé à 25% au-dessus de la valeur du marché par le biais à l'API déjà implémentée. Cette API devrait être programmée pour calculer la moyenne flottante de 25 % au-dessus du prix du marché sur la base du cours vendeur des derniers jours et des dernières semaines, en équilibrant les "pump" et "dump" à court terme.  

La formule et le niveau de pourcentage seraient fixés dans des assemblées par consensus, comme cela a été le cas jusqu'à présent pour le taux officiel. Les taux de change supplémentaires entre le Fairo et d'autres monnaies que le FairCoin ne seraient fixés que de manière décentralisée, par les nœuds locaux ou par le biais d'une coordination entre plusieurs nœuds locaux dans une biorégion.  

## Fairo: Proposition 3  

Le taux actuel du Fairo suivrait une moyenne comprise entre 1 FairCoin == 1 Fairo et une moyenne flottante de 25% au-dessus de la valeur marchande jusqu'à ce que la moyenne flottante de 25% au-dessus de la valeur marchande soit supérieure à 1 FairCoin == 1 Fairo. Au-dessus de 1 FairCoin == 1 Fairo, s'appiquerait alors les mêmes choses que dans la Proposition 2 de Fairo.  

Techniquement, cela fonctionne déjà dans les portemonnaies mobiles et ordinateurs actuels, car le Fairo est déjà listé comme une devise disponible dans les portemonnaies. C'est juste le taux de valeur entre FairCoin et Fairo qui serait changé à 25% au-dessus de la valeur du marché par le biais à l'API déjà implémentée. Cette API devrait être programmée pour calculer la moyenne entre 1 FairCoin == 1 Fairo et une moyenne flottante de 25% au-dessus de la valeur marchande ou la moyenne flottante de 25% au-dessus du prix du marché, le plus élevé des deux étant retenu, sur la base du cours vendeur des jours et semaines précédents.  

La formule et le niveau de pourcentage seraient fixés dans des assemblées par consensus, comme cela a été le cas jusqu'à présent pour le taux officiel. Les taux de change supplémentaires entre le Fairo et d'autres monnaies que le FairCoin ne seraient fixés que de manière décentralisée, par les nœuds locaux ou par le biais d'une coordination entre plusieurs nœuds locaux dans une biorégion.  



Deutsch:
# Fairo Vorschlag  

Dies schlägt eine Weiterentwicklung des Fairo vor, wie er heute hier existiert: https://fairo.exchange/ und in der fairen Wirtschaft zwischen den lokalen FairCoop-Knotenpunkten in der Schweiz getestet wurde.  

Dieser Vorschlag legt nahe, dass es an der Zeit ist, den Kern der FairCoin-Wirtschaft stärker vom Finanzsystem zu trennen, welches wir ersetzen wollen. Solange wir in unserem Denken weiterhin für unsere faire Wirtschaft den Euro als zentrale Messgröße für den Wert sehen, bleiben wir zu direkt in die Spannungen der uns umgebenden systemischen Realität eingebunden. Statt der Abhängigkeit unserer fairen Wirtschaft von einem Wechselkurs zum Euro verlagern wir daher unser Denken auf den Fairo als unabhängiges Maß für den Wert.  

Der Fairo ist eine vorgeschlagene Maßeinheit für die Kaufkraft. Der Wert, den 1 Fairo darstellt, basiert auf der Kaufkraft von 20 FairCoin am Ende des Jahres 2016, sollte aber vor allem als ein Tausendstel der durchschnittlichen zukünftigen Lebenshaltungskosten für einen Monat angesehen werden. Während der Wert eines Fairo stabil ist, würden wir Konsensuskurse für den Austausch zwischen Fairo und FairCoin im Vertrauensnetz der lokalen Knoten von FairCoop festlegen. Daher kann es in verschiedenen Bereichen eine Vielzahl von Tarifen für unterschiedliche wirtschaftliche Realitäten geben.  Bei der Festlegung der Fairo-Kurse würden wir in berechneten Bandbreiten dem Markt folgen, entweder durch direkte Verknüpfung des Fairo-FairCoin-Wechselkurses durch Konsens mit diesen berechneten Bandbreiten oder durch eine stärkere Berücksichtigung dieser Bandbreiten als Leitfaden für bewährte Praktiken bei der Anpassung von Wechselkursen in Konsensversammlungen.  

Ein Grundsatz der Fairo-Sätze wäre, dass sie FairCoin über dem wahrgenommenen Marktwert außerhalb der fairen Wirtschaft bewerten. Dies verschafft den Marktteilnehmern, die FairCoin in der fairen Wirtschaft ausgeben, einen Wettbewerbsvorteil und schafft einen Nachteil, wenn FairCoin außerhalb der fairen Wirtschaft ausgegeben oder ausgetauscht wird. Dies ist ein Weg, wie FairCoin als Membrane dienen kann, um die faire Wirtschaft vor der Mehrwertabschöpfung des kapitalistischen Systems zu schützen. 
Die Lücke, die zwischen der Bewertung von FairCoin in der fairen Wirtschaft und der außerhalb des kapitalistischen Marktes bestehen kann, muss im Verhältnis zur Stärke des Vertrauensnetzes der fairen Wirtschaft gehalten werden. Wenn die Lücke zu groß wird, bricht das Vertrauensnetz zusammen. Auch wenn das Vertrauensnetz geschwächt wird, muss die zu erhaltende Lücke reduziert werden. Die richtige Lücke ist nur schwierig meßbar, deshalb ist die Entwicklung einer guten Verwaltungskultur wichtig.

Der Fairo-Satz von FairCoin würde nach oben an den Markt angepasst, aber auch nach unten korrigiert. Anstatt die faire Wirtschaft der vollen Marktschwankung auszusetzen, würde sie über einem mittel- oder kurzfristigen Mittelwert des Marktes schweben, ähnlich dem, was mit dem "Ask-Price" des FreeVision-Vorschlags vorgeschlagen wird. 

Der Fairo-Kurs wird für FairCoin-Transaktionen innerhalb der fairen Wirtschaft und für den Austausch zwischen FairCoin und anderen sozialen Währungen und gegenseitigen Kreditsystemen verwendet. Dies würde einen großen Wandel im Denken über den Wert von Beiträgen und Ressourcen in der fairen Wirtschaft im Allgemeinen und den Wert von FairCoin im Besonderen bedeuten. Die Transaktion in Fairo befreit uns von der Geisteshaltung, die das Geldsystem der Zentralbank und der Wettbewerbscharakter der verschiedenen Volkswirtschaften der Welt auferlegt. Da der Wert von FairCoin schlecht messbar ist, soll er stattdessen anhand des klar verständlichen Konzepts der monatlichen Lebenshaltungskosten gedacht werden. So kann man mit unterschiedlichen lokalen Gegebenheiten und wirtschaftlichen Realitäten auf eine viel natürlichere und entspanntere Weise umgehen. Ein Schlüssel zur Entwicklung einer gesunden, fairen Wirtschaft.  

Das Problem in unserer gegenwärtigen Realität der fairen Wirtschaft mit FairCoin ist die Notwendigkeit, die Unklarheit der Bewertung von FairCoin, die Kluft zwischen den verschiedenen Wechselkursen und die Notwendigkeit, eine begrenzte Liquidität für den Umtausch zu akzeptieren, zu rechtfertigen. Mit dem Fairo werden diese Bewertungsunterschiede der FairCoins zu einer Korrektur der marktüblichen Bewertungsart. Mit dem Fairo sind wir in der Lage, die unklare Bewertung in eine Tugenden der Funktionsweise unserer fairen Wirtschaft umzuwandeln. Eine Realität, von der wir unterbewusst immer wussten, dass sie wahr ist, aber ohne dem Fairo war sie schwer zu erklären und zu rechtfertigen. Der Fairo löst diese Spannung.


### Nachhaltige Umtauschmöglichkeiten  

In gewissem Maße würden wir wollen, dass die Marktteilnehmer in fairen Wirtschaft ihre FairCoins in Zentralbankengeld umtauschen können, um faire externe Bedürfnisse zu decken. Die Möglichkeit, in unfaire Währungen umzutauschen, ist nur insofern nachhaltig, als die faire Wirtschaft in der Lage ist, Wert aus dem kapitalistischen System zu gewinnen. Nachhaltige Wege dazu sind:  
    a) Faire Wirtschaftsteilnehmer, die Dienstleistungen für die unfaire Wirtschaft mittels Strukturen wie FreedomCoop anbieten, können durch die von ihnen bezahlten Gebühren zur Finanzierung des Umtauschbedarfs beitragen. 
    b) Menschen mit einer Lebensrealität und einem stabilen Einkommen in der unfairen Wirtschaft, aber mit Interesse und Sympathie für die faire Wirtschaft können einen monatlichen Betrag an unfairer Währung für den Umtauschbedarf beisteuern, im Umtausch für FairCoin, welche sie in der fairen Wirtschaft ausgeben wollen.
    c) Faire Investoren, die unfaire Währung tauschen, um FairCoin zu kaufen, ohne die Absicht, jemals wieder in eine unfaire Währung umzutauschen, und stattdessen planen, die FairCoins in faire Wirtschaftsprojekte zu investieren.  

Aus solchen Umtauschgeschäften erhaltenes unfaires Geld, sollte zwischen der Verwendung für den fairen Rücktausch und dem Kauf von FairCoin zu einem tiefen Preis vom unfairen Markt, aufgeteilt werden.   


### Ausgewogenheit des fairen Umtauschs durch Umtauschspannen (FairPoints Vorschlag)  

Dies beschreibt eine vorgeschlagene Methode zur Berechnung der Wechselkurse zwischen einer fairen Währung und unfairen Währungen (zBsp. Zentralbankgeld): 

In erster Linie wären es die Marktbedingungen, die die Fähigkeit zum Austausch in unfaire Währungen einschränken, aber wir würden auch den Zugang zum Umtausch auf der Grundlage der Fairness der Marktteilnehmer differenzieren wollen, um ethisches und faires Marktverhalten zu fördern. Eine einfache Möglichkeit, diese Fairness zu messen, könnte durch die Einführung eines FairPoint-Systems erreicht werden, wie hier vorgeschlagen: Je mehr FairPoints ein Marktteilnehmer hat, desto höher ist der Wechselkurs, auf den er beim Umtausch in eine unlautere Währung Zugriff hat. Das Konzept ist so einfach, dass es in einer Weise gestartet werden kann, die auf der Selbsteinschätzung der Marktteilnehmer beruht und die der Selbstregulierung durch Peer Review überlassen wird.  

Die FairPoints sind in 3 Fairness-Kategorien unterteilt. Für jede dieser Kategorien gibt es 3 Kriterien. Für jede Kategorie kann man von 0 bis 2 Punkte erhalten:  

Fairness-Kategorie A 
- Kriterium a) 
- Kriterium b) 
- Kriterium c)  

Fairness-Kategorie B 
- Kriterium a) 
- Kriterium b) 
- Kriterium c)  

Fairness-Kategorie C 
- Kriterium a) 
- Kriterium b) 
- Kriterium c)  

Punktzahl: Für A, B und C: 
    - 1 Berechnungspunkt für 2 von 3 Kriterien 
    - 2 Berechnungspunkte für alle 3 
für insgesamt maximal 6 Berechnungspunkte 

FairPoints werden berechnet, indem das Total der Berechnungspunkte durch 2 dividiert und für eine FairPoint-Bewertung zwischen 0 und 3 abgerundet werden.  

Marktteilnehmer mit 3 FairPoints können beim Wechsel zu einer unfairen Währung zu 90% des Fairo-Kurses tauschen. Diejenigen mit 2 FairPoints bei 80% und die mit 1 FairPoint bei 70%. Diejenigen mit 0 FairPoints können kein Geld innerhalb des Vertrauensnetzwerks tauschen und müssen dies auf dem externen Markt tun. 

Marktteilnehmer mit 3 FairPoints können FairCoin zu 70% des Fairo-Kurses bei der Umrechnung ihrer Euros kaufen. Diejenigen mit 2 FairPoints bei 80% und die mit 1 FairPoint bei 90% und diejenigen mit 0 FairPoint zum vollen Wert, also 100%.  

Dies erfordert nach wie vor ein funktionierendes Vertrauensnetz in der fairen Wirtschaft, um zu verhindern, dass in grösserem, unfairem Ausmass Faircoins akzeptiert werden, welche ausserhalb der fairen Wirtschaft billig gekauft wurden. Mit einem erweiterten Block-Explorer könnten wir im Laufe der Zeit die Transparenz erhöhen und damit die Notwendigkeit verringern, sich auf das Netz des Vertrauens selbst zu verlassen.  

Separates Pad für die Entwicklung des FairPoints-Vorschlags: https://board.net/p/fairpoints   


## Fairo Vorschlag 1  

Die aktuelle Fairo-Rate würde auf 1 FairCoin == 1 Fairo heruntergesetzt.  

Technisch funktioniert dies bereits in den aktuellen mobilen und Desktop-Wallets, da der Fairo bereits als verfügbare Währung in den Wallets aufgeführt ist. Es ist nur die Rate, die durch das bereits implementierte API auf 1:1 geändert wird.  

Der zukünftige Zinssatz würde im Konsens durch die Vollversammlung festgelegt werden, wie es bisher für den offiziellen Zinssatz der Fall war. Zusätzliche Wechselkurse zwischen Fairo und anderen Währungen als FairCoin würden nur dezentral, über die Lokalgruppen oder durch Koordination zwischen mehreren Lokalgruppen in einer Bio-Region festgelegt.  


## Fairo-Vorschlag 2  

Der aktuelle Fairo-Satz würde auf einen variablen Mittelwert von 25% über dem Marktwert angepasst.  

Technisch funktioniert dies bereits in den aktuellen mobilen und Desktop-Wallets, da der Fairo bereits als verfügbare Währung in den Wallets aufgeführt ist. Es ist nur die Rate, die durch das bereits implementierte API auf den berechnete Wert gesetzt wird. Dieses API müsste programmiert werden, um den variablen Mittelwert von 25% über dem Marktpreis basierend auf dem "Ask" Preis der letzten Tage und Wochen zu berechnen, wobei kurzfristige Pumps und Dumps ausgeglichen werden.  

Die Formel und das Prozentniveau würden im Konsens durch die Vollversammlung festgelegt werden, wie es bisher für den offiziellen Kurs der Fall war. Zusätzliche Wechselkurse zwischen Fairo und anderen Währungen als FairCoin würden nur dezentral, über die Lokalgruppen oder durch Koordination zwischen mehreren Lokalgruppen in einer Bio-Region festgelegt.


## Fairo-Vorschlag 3  

Der aktuelle Fairo-Satz würde einem Mittelwert zwischen 1 FairCoin == 1 Fairo und einem variablen Mittelwert von 25% über dem Marktwert folgen, bis der variable Mittelwert von 25% über dem Marktwert höher als 1 FairCoin == 1 Fairo liegt. Oberhalb von 1 FairCoin == 1 Fairo wäre es dann dasselbe wie bei Fairo Vorschlag 2.  

Technisch funktioniert dies bereits in den aktuellen mobilen und Desktop-Wallets, da der Fairo bereits als verfügbare Währung in den Wallets aufgeführt ist. Es ist nur die Rate, die durch die bereits implementierte API auf den berechnete Wert gesetzt wird. Diese API müsste programmiert werden, um den Mittelwert zwischen 1 FairCoin == 1 Fairo und einem gleitenden Mittelwert von 25% über dem Marktwert oder dem gleitenden Mittelwert von 25% über dem Marktpreis, je nachdem, welcher höher ist, basierend auf dem "Ask" Preis der letzten Tage und Wochen zu berechnen.  

Die Formel und das Prozentniveau würden im Konsens durch die Vollversammlung festgelegt werden, wie es bisher für den offiziellen Kurs der Fall war. Zusätzliche Wechselkurse zwischen Fairo und anderen Währungen als FairCoin würden nur dezentral, über die Lokalgruppen oder durch Koordination zwischen mehreren Lokalgruppen in einer Bio-Region festgelegt.


# Propuesta Fairo
Traducción realizada con el traductor www.DeepL.com/Translator


Esto propone una evolución del Fairo tal y como existe actualmente aquí: https://fairo.exchange/ y ha sido experimentado en la economía justa entre los nodos locales de FairCoop en Suiza.

Esta propuesta sugiere que es hora de desconectar con más fuerza el núcleo de la economía de FairCoin del sistema financiero que queremos sustituir. Mientras sigamos pensando en nuestra economía justa en términos del Euro como la medida central del valor, nos mantendremos en contacto demasiado directo con las tensiones de la realidad sistémica que nos rodea. En lugar de la dependencia de nuestra economía justa de un tipo de cambio al euro, cambiamos nuestro pensamiento hacia Fairo como una medida independiente de valor. 

El Fairo es una unidad de medida propuesta para el poder adquisitivo. El valor que representa 1 Fairo se basa en el poder adquisitivo de 20 FairCoin a finales del año 2016, pero lo que es más importante, debe considerarse equivalente a una milésima parte del coste de la vida básico medio futuro durante un mes. Aunque el valor de un Fairo es estable, fijaríamos tasas de consenso para el intercambio entre Fairo y FairCoop dentro de la red de confianza de los nodos locales de FairCoop. Por lo tanto, puede haber una diversidad de tasas para diferentes realidades económicas en diferentes áreas. 

Para establecer los tipos de cambio de Fairo, se seguirían las bandas calculadas de seguimiento del mercado, ya sea vinculando directamente el tipo de cambio de Fairo-FairCoin por consenso a estas bandas calculadas, o siguiendo de forma más general estas bandas como guía de las mejores prácticas al ajustar los tipos de cambio en las asambleas de consenso. 

Un principio de las tasas de Fairo sería que valoran FairCoin por encima del valor de mercado percibido fuera de la economía justa. Esto da una ventaja competitiva a los participantes del mercado que gastan FairCoin en la economía justa y crea una desventaja cuando FairCoin se gasta o intercambia fuera de la economía justa. Esta es una de las formas en que FairCoin puede servir como una membrana para proteger la economía justa de la extracción de valor del sistema capitalista.
La brecha que se puede mantener entre la valoración de FairCoin en la economía justa y la del mercado capitalista debe mantenerse en relación con la fuerza de la red de confianza en la economía justa. Si esa brecha se hace demasiado grande, la red de confianza se romperá. Además, cuando la red de confianza se debilita, es necesario reducir la brecha que se puede mantener. Es difícil medir la brecha correcta que debe mantenerse para un funcionamiento adecuado en equilibrio con la fuerza de la red de confianza, razón por la cual es importante el desarrollo de una cultura de buena gobernanza.  

La tasa Fairo de FairCoin se ajustaría al alza con el mercado, pero también se corregiría a la baja. En lugar de exponer la economía justa a la fluctuación total del mercado, se mantendría por encima de una media del mercado a medio o corto plazo, similar a lo que se sugiere con el precio de venta de la propuesta de FreeVision. 
La tasa Fairo se utilizaría para las transacciones de FairCoin dentro de la economía justa y para el intercambio entre FairCoin y otras monedas sociales y sistemas de crédito mutuo. Esto representaría un cambio importante en la forma de pensar sobre el valor de las contribuciones y los recursos en la economía justa en general y el valor de FairCoin en particular. Transaccionar en Fairo nos libera del estado de ánimo impuesto por el sistema monetario del banco central y de la naturaleza competitiva de las diferentes economías del mundo. Dado que el valor de FairCoin es difícil de medir, debería considerarse en términos del concepto claramente comprensible del coste de la vida mensual. Esto nos permite tratar con las diferentes condiciones locales y realidades económicas de una manera mucho más natural y relajada. Una clave para desarrollar una economía sana y justa.

El problema en nuestra actual realidad de economía justa con FairCoin es la necesidad de justificar la ambigüedad de cómo se valora FairCoin, la diferencia entre los diferentes tipos de cambio y la necesidad de aceptar una liquidez limitada en los mercados de cambio. Con el Fairo, estas diferencias de valoración se convierten en una corrección de la valoración mal definida con el dinero del banco central. Con el Fairo podemos convertir estos problemas en virtudes de la forma en que funciona nuestra economía justa. Una realidad que inconscientemente siempre supimos que era cierta, pero sin el Fairo era difícil de explicar y justificar.

### Fuentes de intercambio sostenibles

Hasta cierto punto, nos gustaría que los participantes de la economía de mercado justa pudieran intercambiar dinero de FairCoin por dinero del banco central para cubrir las necesidades externas justas. Ofrecer la capacidad de cambiar divisas injustas sólo es sostenible en la medida en que la economía justa sea capaz de extraer valor del sistema capitalista. Las formas sostenibles de hacerlo lo son:

a) Los participantes de la economía justa que ofrecen servicios a la economía injusta a través de herramientas como FreedomCoop pueden contribuir con las cuotas recaudadas para financiar las necesidades de intercambio.

b) Las personas con una realidad de vida e ingresos estables en la economía injusta pero con interés y simpatía por la economía justa pueden contribuir con una cantidad mensual de moneda injusta para las necesidades de cambio, a cambio de FairCoin que tienen la intención de gastar en la economía justa.
c) Inversionistas justos que dan moneda injusta para comprar FairCoin sin la intención de volver a cambiar a una moneda injusta, planeando en su lugar invertir las monedas FairCoins en proyectos de economía justa.

El dinero injusto recibido de tales intercambios debe dividirse entre el uso para el canje justo y la compra de FairCoin a bajo precio en el mercado injusto.

### Equilibrar el intercambio justo a través de las bandas de intercambio (Propuesta FairPoints)

Esto describe una forma propuesta de calcular los tipos de cambio entre cualquier moneda justa y monedas injustas (como el dinero del banco central):

En primer lugar, serían las condiciones de mercado las que limitarían la capacidad de proporcionar divisas injustas, pero también quisiéramos diferenciar el acceso a las bolsas sobre la base de la equidad de los participantes en el mercado, con el fin de fomentar un comportamiento ético y justo en el mercado. Una forma sencilla de medir esta imparcialidad podría lograrse con la adición de un sistema FairPoint como el que aquí se propone: Cuantos más FairPoints tenga un participante en el mercado, mayor será el tipo de cambio al que tendrá acceso cuando cambie a una divisa injusta. El concepto es bastante simple, de modo que podría iniciarse de una manera que se base en la autoevaluación de los participantes en el mercado, quedando a cargo de la autorregulación a través de la revisión por pares.

Los FairPoints se dividen en 3 categorías de equidad. Para cada una de estas categorías hay 3 criterios. Por cada categoría se pueden obtener de 0 a 2 puntos: 

Equidad Categoría A
- Criterio a)
- Criterios b)
- Criterios c)

Equidad Categoría B
- Criterio a)
- Criterios b)
- Criterios c)

Equidad Categoría C
- Criterio a)
- Criterios b)
- Criterios c)

Conteo de puntos: 
Para A, B y C:
- 1 cuenta por 2 de cada 3
- 2 cuentas para los 3
para un máximo total de 6 cuentas

Los FairPoints se calculan dividiendo el total de puntos por 2 y redondeando hacia abajo para una calificación de FairPoint entre 0 y 3. 

Los participantes del mercado con 3 FairPoints pueden cambiar al 90% de la tasa Fairo al volver a cambiar a una moneda injusta. Aquellos con 2 FairPoints al 80% y aquellos con 1 FairPoint al 70%. Aquellos con 0 FairPoints no pueden cambiar dinero dentro de la red de confianza y deben hacerlo en el mercado externo. 

Los participantes del mercado con 3 FairPoints pueden comprar FairCoin al 70% de la tasa Fairo al convertir sus euros. Aquellos con 2 FairPoints al 80% y aquellos con 1 FairPoint al 90% y aquellos con 0 FairPoint a valor completo, es decir, 100%.  

Esto todavía requiere una red de confianza en funcionamiento en la economía justa para prevenir cualquier aceptación injusta a gran escala de monedas baratas compradas fuera de la economía justa.
Con un explorador de bloques mejorado, con el tiempo podríamos aumentar la transparencia y, por tanto, reducir la necesidad de confiar en la propia red de confianza.

Pad para desarrollar la propuesta de FairPoints: https://board.net/p/fairpoints

## Propuesta Fairo 1

La tasa actual de Fairo se ajustaría a 1 FairCoin == 1 Fairo. 

Técnicamente, esto ya funciona en los monederos móviles y de sobremesa actuales, ya que el Fairo ya figura como moneda disponible en los monederos. Es sólo la tasa que se cambiaría a 1:1 a través de la API ya implementada.

La tasa futura se fijaría en asambleas de consenso, como ha sido el caso de la tasa oficial hasta ahora. Los tipos de cambio adicionales entre Fairo y otras monedas distintas de FairCoin sólo se establecerían de forma descentralizada, a través de nodos locales o de la coordinación entre múltiples nodos locales en una bio-región.

## Propuesta Fairo 2

La tasa Fairo actual se ajustaría a una media flotante del 25% por encima del valor de mercado. 

Técnicamente, esto ya funciona en los monederos móviles y de sobremesa actuales, ya que el Fairo ya figura como moneda disponible en los monederos. Es sólo la tasa que se cambiaría a un 25% por encima del mercado a través de la ya implementada API. Esa API tendría que ser programada para calcular la media flotante del 25% por encima del precio de mercado basado en el precio de venta de los últimos días y semanas, compensando las bombas y vertederos a corto plazo.

La fórmula y el nivel de porcentaje se establecerían en asambleas de consenso, como ha sido el caso hasta ahora de la tasa oficial. Los tipos de cambio adicionales entre Fairo y otras monedas distintas de FairCoin sólo se establecerían de forma descentralizada, a través de nodos locales o de la coordinación entre múltiples nodos locales en una bio-región.

## Propuesta Fairo 3

La tasa Fairo actual seguiría una media entre 1 FairCoin == 1 Fairo y una media flotante 25% por encima del valor de mercado hasta que la media flotante 25% por encima del valor de mercado sea superior a 1 FairCoin == 1 Fairo. Por encima de 1 FairCoin == 1 Fairo entonces sería lo mismo que Fairo Proposal 2.

Técnicamente, esto ya funciona en los monederos móviles y de sobremesa actuales, ya que el Fairo ya figura como moneda disponible en los monederos. Es sólo la tasa que se cambiaría al valor calculado a través de la API ya implementada. Esa API tendría que ser programada para calcular la media entre 1 FairCoin == 1 Fairo y una media flotante 25% por encima del valor de mercado o la media flotante 25% por encima del precio de mercado, lo que sea más alto, basado en el precio de venta de los últimos días y semanas.

La fórmula y el nivel de porcentaje se establecerían en asambleas de consenso, como ha sido el caso hasta ahora de la tasa oficial. Los tipos de cambio adicionales entre Fairo y otras monedas distintas de FairCoin sólo se establecerían de forma descentralizada, a través de nodos locales o de la coordinación entre múltiples nodos locales en una bio-región.



Comments

Comments, questions and answers can be found here:
https://board.net/p/fairo-porposal-questions



#### Question:
> Fairo should be thought of as equivalent to one thousandth of the mean future basic cost of living for one month.

That seems rather hard to calculate, but more important, it tends to maintain differences across the world. On Europe, where people is used to "be fine" with different amounts (that vary a lot), and people tend to calculate future needs based on that. https://es.wikipedia.org/wiki/Efecto_de_anclaje or https://en.wikipedia.org/wiki/Anchoring#Difficulty_of_avoiding

#### Answer:
Both are intentional, the challenge of requiring some extent of a consensus on what should be seen as a basic cost of living, as well as the anchoring of the concept of value in the basic cost of living.  

The former comes down to a cultural challenge that is a key ability we need to develop in order to manifest the fair ecosystem and an important characteristic of our federated web of trust. 

The latter, the anchoring, is exactly what is meant with the intended shift of the mindset, away from the concept of fractional reserve banking money, to the concept of what constitutes a basic cost of living and the tension of not being able to pin a definition of such a basic cost of living on any specific formula, instead requiring good governance and culture in the web of trust of the fair ecosystem.

---

#### Question:
> A principle of the Fairo rates would be that they value FairCoin above the perceived market value outside the fair economy. This gives a competitive advantage to market participants that spend FairCoin in the fair economy and creates a disadvantage when FairCoin is spent or exchanged outside the fair economy. This is one way how FairCoin can serve as a membrane to protect the fair economy from the value extraction of the capitalist system.

I think that the effect can be read on the opposite direction, as we are giving people outside of the system more power to buy products (worktime) from the fair economy.

#### Answer:
To the extent that outside people would buy FairCoin on the unfair market to obtain worktime from the fair ecosystem, they would actually, through that action, make a first step of joining the fair ecosystem. Although they would benefit, they would be investing into the fair ecosystem by helping to bring fair currency that has leaked out into the unfair market back into the fair market. The quoted aspect is only *one* way for that protective membrane to function. There are other aspects of this membrane we need to develop in parallel. 

For example, we can create tools to use the blockchain to verify the origin of FairCoins in order prevent such abuse or that we build tools (like FreedomCoop) that encourage outside customers to pay for fair economy worktime in unfair currency, part of which can then be used for exchange needs and for purchasing cheap FairCoin from the unfair market. 

The full membrane is made up of a mix of different aspects and each one of them does not need to be perfect. There can be leakage. As long as the overall net effect is that the fair economy can extract more value from the unfair market then that the outside system can extract from us, the membrane is functioning in our favor. 

---

#### Question:
> The Fairo rate of FairCoin would be adjusted up with the market

I think that it is a bit confussing thinking on the Fairo valuation while it is not a currency itself. As the FairCoin is the currency, it cannot be valued as the Fairo, and the rate of value with worktime (that is the base of value in marxism) cannot be fixed. There is something I cannot understand here.

#### Answer:
We are setting the rate of FairCoin, or that of some other currency, never that of Fairo itself. 

We are not setting the rate of Fairo as such, ever, since the value of Fairo is the stable anchor. We can have living costs that we consider to be over or under the basic mean, and what constitutes "basic living" can evolve over time, but we can still maintain a consensus about what we consider the mean of such a basic living cost. That is what constitutes the anchor. 

Once the mindset shifts and we set such an anchor for ourselves in our minds, the confusion goes away.

---

#### Question:
> The future rate would be set in consensus assemblies like it has been the case for the official rate so far. Additional exchange rates between Fairo and other currencies than FairCoin would only be set in a decentralised way, through local nodes or coordination between multiple local nodes in a bio region.

There is no way for the wallet to check a descentralized exchange rate (I think that it is the most problematic technical issue in FairCoin actually).

#### Answer:
The wallet already has a Fairo rate and it would have only one Fairo rate that we set globally. The API that now also provides the wallet with all the different national currency rates could provide instead the different exchange rates set by either local nodes or through coordination in bioregions. This could include rates to unfair currency, such as a euro rate set by the Leipzig local node and a euro rate set by the Athens local node, and it can include rates for fair currencies, such as the Eco or a local LETS exchange rate. 

Btw, when switching from the official euro rate to using the Fairo was first discussed almost two years ago, we even talked about making the Fairo rate itself a chain parameter, to make it an uncorruptible part of the governance process of the FairCoin blockchain instead of relying on a centralized API, just in case you were also meaning the decentralization challenge in this way.

---

#### Question:
Do you see that Fairo rate is better be implemented in the newly proposed FairCash? 
or that is aimed at FairCoin, thus no need to have FairCash?
 
#### Answer:
It for sure aims at fixing the situation we have in FairCoin itself. Of course, Fairo as a value reference could then be used by any fair currency or mutal credit system, either directly or by fixing an exchange rate to it.

---

#### Question:
I believe FairCoin should be focused mainly at that second sector, while FairCash, Fairo rate and possibly other social currencies we connect to, should stay in the first sector... more protected and manageable by the community(ies)
 
#### Answer:
The way I have always understood FairCoin, it aims to be the very outer boundary where the fair ecosystem interacts with the unfair money system. We should make FairCoin the protective membrane around an abundance of fair currencies, mutual credit and exchange systems. 

I think that in order for FairCoin to fullfill that membrane functionality, we need the concept of the Fairo. And without that membrane functionality, the extraction of value from the capitalist system will not work either.  

There is nothing wrong with creating more fair currency systems, but when it comes to FairCoin, we need to fix FairCoin itself. We can't fix FairCoin by adding FairCash.

---

#### Question:
Why can we not do the same thing just with the Euro rate. I don't really see why you think that we need the concept of Fairo.
 
#### Answer:
I think we are underestimating the importance of reanchoring the economic thinking in the fair economy, away from measuring value based on fractional reserve banking money, if we want to build an ecosystem that is a replacement for it. The Fairo is a very bold way of telling the Euro and central bank money to fuck off and to manifest that shifted mindset.

---

#### Question:
35% above market price FairCoin rate is interesting. So we will constantly be translating Fairos to different amounts of FairCoin according to current exchange market rate of FairCoin +35%. That's helpful but I wonder how this works with assemblies both local and global - we know from assemblies that it can be extremely difficult to get consensus. Although perhaps its more viable at local node level.

Is the idea just a rough formula in which you are inevitably letting politics intervene? Or is it automated? If it is subject to assembly consensus there is always the possibility that someone at an assembly takes a different view and blocks, so we will still be locked with the previous FairCoin price, which could go from 35% higher or lower than market to 200% higher or lower in a relatively short (say week long) period. 

#### Answer:
With the way the Fairo/FairCoin exchange rate is defined now in the Jura since the FairHackathon at the beginning of June 2019, we have a (at least local) consensus on a formula of how the rate is calculated and adjusts automatically. So, the Fairo to FairCoin relation would likely be set with little need for adjusting it and having to find consensus on rate adjustements again and again. The adjustements would be automatic based on the formula.
The need for local consensus for example in London would instead be for setting local rates that would divert from the 1 Fairo == 1 GBP definition, which, once agreed, is then not subject the value fluctuations of faircoin. So, that local consensus would not need to be rediscussed often.

---

#### Question:
FairCoin will still potentially fluctuate quite a lot from month to month in terms of market price so it does not really help in terms of stable pricing. So while the price of Fairo may anchor, the price of FairCoin will still be prone to month by month fluctuation and the exchange will ultimately still be in FairCoin. Won't this still lead to confusion between market price and FairCoin community price both of which will be fluctuating? (While the Fairo helps to anchor value it does not ultimately resolve this)
 
#### Answer:
On the one hand the Fairo does bring a very high stability when it comes to price indication, which already solves a large part of the need for stability. It additionally brings a lot of stability in regards to disconnecting value definitions from the central bank money, with the Fairo staying at its valuation in the case that central bank money should tank/collaps - an aspect of stability that should not be underestimated. But third it also brings stability in the exchange values to and from the central bank money, as it does not get effected by short term market spikes and drops, averaging out rising periods with a short term mean and declining periods with a mid term mean. So, while not bringing total stability in the sense of pegging FairCoin to a central bank currency, it does bring a lot of stability when it comes to taking out any major volatility. A really stable relation with central bank money we should not desire anyway, as that would then also bind us to the central bank money, which clearly can not be in the interest of the FairCoin and FairCoop projects.

---

#### Question:
I was thinking that within the Fairo proposal in effect you are downplaying FairCoin as a store of value. Since it will still fluctuate with market rates. FairCoin if you like would become the fluid for the anchoring of exchanges around the Fairo. In terms of the philosophy of money and the idea that FairCoin has three functions:
    - funding the movement
    - means of fair exchange
    - store of value
The Fairo really solidifes and prioritises the means of fair exchange function (although you still have the problem of cheap market FairCoin purchases, even if it reduces the problem). But the other two aspects of FairCoin's functions (in theory more than in practice at the moment) are perhaps downplayed by the Fairo. Would you agree with that account?
 
#### Answer:
No, with the change of the default valuation of FairCoin to be based on Fairo, both the functions of funding the movement and of serving as a store of value can work at least as well as with the current Euro bound official rate, imo. But if I would pick the three most major functions of FairCoin, I would say they are:
    - serving as a membrane to hinder capitalist value extraction from the fair economy
    - translocal exchange across different fair economies, social currencies and different regions
    - store of value

---


