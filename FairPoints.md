# Balancing fair exchange through exchange bands (FairPoints)

This describes the suggested way of calculating exchange rates between any fair currency and unfair currencies (like central bank money):

Primarily, it is the market conditions that limit the ability to provide exchange to unfair currency, but we also want to differentiate access to exchanges based on the fairness of market participants, in order to encourage ethical and fair market behaviour. One simple way of measuring this fairness is achieved by applying the FairPoint system as described here: The more FairPoints a market participant has, the higher the exchange rate they have access to when exchanging to an unfair currency. The concept is simple enough so that it could be started in a way that relies on self-assessment of the market participants, being left to self regulate through peer review.

The FairPoints are divided into 3 fairness categories. For each of these categories there are 3 criteria. For each category one can get from 0 to 2 points: 
 
Fairness Category A
- Criteria a)
- Criteria b)
- Criteria c)

Fairness Category B
- Criteria a)
- Criteria b)
- Criteria c)

Fairness Category C
- Criteria a)
- Criteria b)
- Criteria c)

Point count: 
For A,B and C:
- 1 count for 2 out of 3
- 2 counts for all 3
for a total maximum of 6 counts

FairPoints are calculated by dividing the total counts by 2 and rounding down for a FairPoint rating between 0 and 3. 

For exchanges to and from unfair currencies, there are three levels of exchange rates, based on the following formulas:

L1 = average(last7DailyMean,FairoRate)
L2 = average(L1,FairoRate)
L3 = average(L2,FairoRate)

Market participants with 3 FairPoints get to exchange at L3 when changing back to an unfair currency. Those with 2 FairPoints at L2 and those with 1 FairPoint at L1.

Market participants with 3 FairPoints get to buy FairCoin at L1 when converting their Euros. Those with 2 FairPoints at L2 and those with 1 FairPoint at L3.

This still requires a functioning web-of-trust in the fair economy in order to prevent the large scale acceptance of cheap faircoins from outside to be accepted in the fair economy in unfair ways.
With an enhanced block explorer, we could over time increase transparency and hence lowering the need for relying on the web of trust itself.


## This concept of "fairpoints" was part of the "Fairo proposal"

See here https://board.net/p/fairo-porposal for that.

So, there are many possibilities for how to define these fairpoint categories and criteria. 
The intention here is to collect/develop some examples.




### FairPoints classification example 1:
 
1) People Care
- in the spirit of Integral Revolution
- is a collective, coop or other form of open group
- cooperative, not competitive
 
2) Earth Care
- in the spirit of the Transition movement
- powered by renewable energy
- vegan
 
3) Fair Share
- in the spirit of Open Source culture
- participating actively in FairCoop
- solidaric





### FairPoints classification example X:
 
1) 
- 
- 
- 
 
2) 
- 
- 
- 
 
3) 
- 
- 
-
